 FROM python:3
 ENV PYTHONUNBUFFERED 1

 RUN mkdir /eastwood
 WORKDIR /eastwood
 ADD requirements.txt /eastwood/
 RUN pip install -r requirements.txt
 ADD . /eastwood/
