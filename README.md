# eastwood
## Тесты:
```
  docker-compose up -d
  docker-compose exec web python manage.py test

```
## Как запускать:
```
  docker-compose up -d
  docker-compose exec web python manage.py migrate
  docker-compose exec web python manage.py createsuperuser
  docker-compose exec web python manage.py fill_db --num 600
  docker-compose exec web python manage.py collectstatic
```

## Открыть в браузере:
```
   http://localhost:8080
```
