import datetime
import factory
from django.urls import reverse
from django.utils.http import urlencode
from django.test import TestCase

from . import factories
from . import models
from .abc_paginator import AbcPaginator, Page


class EmployeesBaseTestCase(TestCase):

    @factory.Faker.override_default_locale('ru_RU')
    def setUp(self):
        super(EmployeesBaseTestCase, self).setUp()
        self.department = factories.Department()
        self.position = factories.Position()
        names = 'Ааааа Абббб Аввввв Бннннн Вааааа Вннннн Гннннн Дннннн Дфывван'
        self.employees = []
        for n in names.split():
            self.employees.append(
                factories.Employee(
                    full_name=f'{n} Ааааа Аааа',
                    department=self.department,
                    position=self.position
                )
            )


class AbcPaginatorTestCase(EmployeesBaseTestCase):

    @staticmethod
    def _create_paginator(max_page_num, queryset=None):
        if queryset is None:
            queryset = models.Employee.objects.all()
        return AbcPaginator(queryset, 'last_name', max_page_num)

    def test_empty(self):
        paginator = self._create_paginator(
            5, queryset=models.Employee.objects.filter(pk=-1).all()
        )
        self.assertEquals(paginator.num_pages, 0)

    def test_max_page_num(self):
        real_num = 5
        paginator = self._create_paginator(real_num)
        self.assertEquals(len(paginator.pages), real_num)

        less_num = 3
        paginator = self._create_paginator(less_num)
        self.assertEquals(len(paginator.pages), less_num)

        greater_num = 10
        paginator = self._create_paginator(greater_num)
        self.assertEquals(len(paginator.pages), real_num)

    def test_num_pages(self):
        paginator = self._create_paginator(3)
        self.assertEquals(len(paginator.pages), paginator.num_pages)

    def test_page(self):
        paginator = self._create_paginator(4)
        page_number = 2
        self.assertIs(
            paginator.page(page_number),
            paginator.pages[page_number - 1]
        )

    def test_pages(self):
        paginator = self._create_paginator(4)
        page_names = list(map(str, paginator.pages))
        self.assertListEqual(page_names, ['А', 'Б - В', 'Г', 'Д'])


class PageTestCase(EmployeesBaseTestCase):

    def test_object_list(self):
        queryset = models.Employee.objects.order_by('last_name').all()
        page = Page(queryset, 1, None, 'В', 'Г')
        last_names = [
            employee.last_name for employee in page.object_list.all()
        ]
        self.assertListEqual(last_names, ['Вааааа', 'Вннннн', 'Гннннн'])


class EmployeesListViewTestCase(EmployeesBaseTestCase):

    def _get(self, params=None):
        url = reverse('employee-list')
        if params:
            querystring = urlencode(params)
            url = f'{url}?{querystring}'
        return self.client.get(url)

    def test_query_page(self):
        response = self._get({'page': 1})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 3)
        self.assertListEqual(
            list(response.context['object_list']), self.employees[:3]
        )

        response = self._get({'page': 2})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['object_list']), 1)
        self.assertListEqual(
            list(response.context['object_list']), self.employees[3:4]
        )

    def test_query_department(self):
        another_department = factories.Department()

        self.employees[0].department = another_department
        self.employees[0].save()

        response = self._get({'department': another_department.pk})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['paginator'].num_pages, 1)
        self.assertEqual(
            list(response.context['object_list']), self.employees[:1]
        )

        response = self._get({'department': self.department.pk})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['paginator'].num_pages, 5)
        self.assertEqual(
            list(response.context['object_list']), self.employees[1:3]
        )

    def test_query_only_working(self):
        for employee in self.employees:
            employee.end_date = datetime.datetime.today()
            employee.save()

        self.employees[0].end_date = None
        self.employees[0].save()

        response = self._get({'only_working': 'on'})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['paginator'].num_pages, 1)
        self.assertEqual(
            list(response.context['object_list']), self.employees[:1]
        )


class EmployeesDetailTestCase(EmployeesBaseTestCase):

    def test_200(self):
        response = self.client.get(
            reverse('employee-detail',
                    kwargs={'pk': self.employees[0].pk})
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['object'], self.employees[0])
