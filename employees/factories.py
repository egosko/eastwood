import datetime
import factory


from . import models


class Employee(factory.django.DjangoModelFactory):
    full_name = factory.Faker('name')
    email = factory.Faker('email')
    birth_date = factory.Faker('past_date')
    phone = factory.Faker('phone_number')
    begin_date = factory.Faker('past_date')
    department = factory.Iterator(models.Department.objects.all())
    position = factory.Iterator(models.Position.objects.all())
    end_date = factory.Iterator([None, datetime.datetime.today()])

    class Meta:
        model = models.Employee
        exclude = ('fio',)


class Department(factory.django.DjangoModelFactory):
    name = factory.sequence(lambda n: f'Отдел {n}')

    class Meta:
        model = models.Department


class Position(factory.django.DjangoModelFactory):
    name = factory.Faker('job')

    class Meta:
        model = models.Position
