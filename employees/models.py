from datetime import datetime
from django.db import models


class Department(models.Model):
    name = models.CharField('Название', max_length=255)

    class Meta:
        verbose_name = 'Отдел'
        verbose_name_plural = 'Отделы'

    def __str__(self):
        return self.name


class Position(models.Model):
    name = models.CharField('Название', max_length=255)

    class Meta:
        verbose_name = 'Должность'
        verbose_name_plural = 'Должности'

    def __str__(self):
        return self.name


class Employee(models.Model):
    last_name = models.CharField('Фамилия', max_length=30)
    first_name = models.CharField('Имя', max_length=15)
    middle_name = models.CharField('Отчество', max_length=30)
    email = models.EmailField('Электронная почта', max_length=255)
    birth_date = models.DateField('Дата рождения')
    phone = models.CharField('Номер телефона', max_length=18)
    begin_date = models.DateField('Дата начала работы', default=datetime.today)
    end_date = models.DateField('Дата окончания работы', blank=True, null=True)
    department = models.ForeignKey('Department',
                                   verbose_name='Отдел',
                                   on_delete=models.CASCADE)
    position = models.ForeignKey('Position',
                                 verbose_name='Должность',
                                 on_delete=models.CASCADE)

    class Meta:
        verbose_name = 'Сотрудник'
        verbose_name_plural = 'Сотрудники'

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        return ' '.join([self.last_name, self.first_name, self.middle_name])

    @full_name.setter
    def full_name(self, value):
        self.last_name, self.first_name, self.middle_name = value.split(' ')[-3:]

