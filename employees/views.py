from django.forms import model_to_dict
from django.utils.functional import cached_property
from django.views.generic import DetailView, ListView

from .abc_paginator import AbcPaginator
from .models import Employee, Department


class EmployeeDetail(DetailView):
    model = Employee

    def get_context_data(self, **kwargs):
        data = super(EmployeeDetail, self).get_context_data(**kwargs)
        data['object_dict'] = model_to_dict(data['object'])
        return data


class EmployeeList(ListView):
    model = Employee
    paginator_class = AbcPaginator
    paginate_by = 7
    paginate_field = 'last_name'
    ordering = ['last_name', 'first_name', 'middle_name']

    def get_queryset(self):
        queryset = super(EmployeeList, self).get_queryset()

        if self.department_id:
            queryset = queryset.filter(
                department_id=self.department_id
            )

        if self.only_working:
            queryset = queryset.filter(end_date__isnull=True)

        return queryset

    def get_context_data(self, *args, **kwargs):
        data = super(EmployeeList, self).get_context_data(*args, **kwargs)
        data['departments'] = Department.objects.all()
        data['selected_department'] = self.request.GET.get('department_id')
        get_copy = self.request.GET.copy()
        data['url_params'] = get_copy.pop('page', True) and get_copy.urlencode()
        return data

    def get_paginator(self, queryset, paginate_by, **_):
        return self.paginator_class(
            queryset, self.paginate_field, paginate_by
        )

    @cached_property
    def department_id(self):
        try:
            return int(self.request.GET.get('department'))
        except (ValueError, TypeError):
            return

    @cached_property
    def only_working(self):
        return self.request.GET.get('only_working') == 'on'
