import factory
from django.core.management.base import BaseCommand

from employees import factories, models


class Command(BaseCommand):
    help = 'Seeds the database.'

    def add_arguments(self, parser):
        parser.add_argument(
            '--num',
            default=300,
            type=int,
            help='The number of fake employees to create.')

    @factory.Faker.override_default_locale('ru_RU')
    def handle(self, *args, **options):
        if not models.Position.objects.exists():
            for _ in range(20):
                factories.Position.create()
        if not models.Department.objects.exists():
            for _ in range(5):
                factories.Department.create()
        for _ in range(options['num']):
            factories.Employee.create()
