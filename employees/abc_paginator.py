
from django.db.models.functions import Substr, Upper
from django.db.models import Count
from django.utils.functional import cached_property


class AbcPaginator(object):

    def __init__(self, object_list, field_name, max_page_num):
        self.field_name = field_name
        self.object_list = object_list
        self.max_page_num = max_page_num

    def _get_letters(self):
        return list(
            self.object_list.values(
                start_letter=Upper(Substr(self.field_name, 1, 1))
            ).annotate(
                num=Count('start_letter')
            ).order_by('start_letter')
        )

    def _create_page(self, number, start_letter, end_letter=None):
        return Page(self.object_list, number, self, start_letter, end_letter)

    @cached_property
    def pages(self):
        result = []
        letters = self._get_letters()

        if not letters:
            return result

        summ = sum(i['num'] for i in letters)
        page_size = summ // self.max_page_num + 1

        while len(letters) > self.max_page_num:
            best = self._find_best_to_merge(letters, page_size)
            self._merge_with_next(letters, best)

        return [
            self._create_page(i, l['start_letter'], l.get('end_letter'))
            for i, l in enumerate(letters, start=1)
        ]

    @staticmethod
    def _merge_with_next(letters, index):
        start_letter = letters[index]
        end_letter = letters[index + 1]

        letters[index: index + 2] = [{
            'start_letter': start_letter['start_letter'],
            'num': start_letter['num'] + end_letter['num'],
            'end_letter':
                end_letter.get('end_letter') or end_letter['start_letter']
        }]

    @staticmethod
    def _find_best_to_merge(letters, page_size):
        best = 0
        max_delta = None
        for i in range(len(letters) - 1):
            current_delta = page_size - (
                letters[i]['num'] + letters[i + 1]['num']
            )

            if max_delta is None or current_delta > max_delta:
                max_delta = current_delta
                best = i

        return best

    def page(self, number):
        return self.pages[number - 1]

    @property
    def num_pages(self):
        return len(self.pages)


class Page(object):

    def __init__(self, object_list, number, paginator, start_letter, end_letter):
        self.object_list = self.get_queryset(
            object_list, start_letter, end_letter
        )
        self.number = number
        self.start_letter = start_letter
        self.end_letter = end_letter
        self.paginator = paginator

    @staticmethod
    def get_queryset(object_list, start_letter, end_letter):
        if end_letter:
            v = f'[{start_letter}-{end_letter}]'
        else:
            v = f'{start_letter}'

        query = object_list.filter(last_name__regex=v)

        return query

    def has_next(self):
        return self.number < self.paginator.num_pages

    def has_previous(self):
        return self.number > 1

    def has_other_pages(self):
        return self.has_previous() or self.has_next()

    def __repr__(self):
        if not self.end_letter:
            return self.start_letter
        else:
            return f'{self.start_letter} - {self.end_letter}'
