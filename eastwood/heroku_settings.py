import dj_database_url

from .settings import *

ALLOWED_HOSTS = ['peaceful-ravine-12628.herokuapp.com']

DATABASES['default'] = dj_database_url.config(conn_max_age=600, ssl_require=True)

MIDDLEWARE.append('whitenoise.middleware.WhiteNoiseMiddleware')
